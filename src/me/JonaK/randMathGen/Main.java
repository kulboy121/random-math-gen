package me.JonaK.randMathGen;

import java.io.*;
import java.util.Random;
import java.util.Scanner;


public class Main {

    final static Scanner in = new Scanner(System.in);
    final static Random rand = new Random();
    final static String[] ops = {"+", "-", "*", "/"};
    static int[] op1;
    static int[] op2;
    static double[] num1;
    static double[] num2;
    static double[] num3;
    static String[] probs;

    static FileWriter qFile;
    static FileWriter aFile;
    final static String newLine = System.getProperty("line.separator");


    public static void main(String[] args) throws IOException {

        System.out.println("Enter how many problems you want:");
        int probNumb = in.nextInt();

        op1 = new int[probNumb];
        op2 = new int[probNumb];
        num1 = new double[probNumb];
        num2 = new double[probNumb];
        num3 = new double[probNumb];
        probs = new String[probNumb];

        qFile = new FileWriter("Questions.txt");
        aFile = new FileWriter("Answers.txt");

        for (int i = 0; i < probNumb; i++) {

            probs[i] = genMath(i);

            qFile.write(probs[i] + newLine);

        }

        for (int i = 0; i < probNumb; i++) {
            String answer = probs[i] + " = " + genAnswers(i);
            aFile.write(answer + newLine);
        }

        qFile.close();
        aFile.close();

        System.out.println("Questions and Answers have been written to their respective files");
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static String genMath(int i) {

        op1[i] = rand.nextInt(4);
        op2[i] = rand.nextInt(4);
        num1[i] = rand.nextInt(100);
        num2[i] = rand.nextInt(100);
        num3[i] = rand.nextInt(100);

        return num1[i] + " " + ops[op1[i]] + " " + num2[i] + " " + ops[op2[i]] + " " + num3[i];

    }

    private static double genAnswers(int i) {
        double probAnswer;
        double temp;
        double n1 = num1[i];
        double n2 = num2[i];
        double n3 = num3[i];

        switch (op1[i]) {
            case 0:
                switch (op2[i]) {
                    case 0:
                        probAnswer = n1 + n2 + n3;
                        break;
                    case 1:
                        probAnswer = n1 + n2 - n3;
                        break;
                    case 2:
                        temp = n2 * n3;
                        probAnswer = n1 + temp;
                        break;
                    case 3:
                        temp = n2 / n3;
                        probAnswer = n1 + temp;
                        break;
                    default:
                        probAnswer = -1.0;
                        break;
                }
                break;
            case 1:
                switch (op2[i]) {
                    case 0:
                        probAnswer = n1 - n2 + n3;
                        break;
                    case 1:
                        probAnswer = n1 - n2 - n3;
                        break;
                    case 2:
                        temp = n2 * n3;
                        probAnswer = n1 - temp;
                        break;
                    case 3:
                        temp = n2 / n3;
                        probAnswer = n1 - temp;
                        break;
                    default:
                        probAnswer = -1.0;
                        break;
                }
                break;
            case 2:
                switch (op2[i]) {
                    case 0:
                        temp = n1 * n2;
                        probAnswer = temp + n3;
                        break;
                    case 1:
                        temp = n1 * n2;
                        probAnswer = temp - n3;
                        break;
                    case 2:
                        temp = n1 * n2;
                        probAnswer = temp * n3;
                        break;
                    case 3:
                        temp = n1 * n2;
                        probAnswer = temp / n3;
                        break;
                    default:
                        probAnswer = -1.0;
                        break;
                }
                break;
            case 3:
                switch (op2[i]) {
                    case 0:
                        temp = n1 / n2;
                        probAnswer = temp + n3;
                        break;
                    case 1:
                        temp = n1 / n2;
                        probAnswer = temp - n3;
                        break;
                    case 2:
                        temp = n1 / n2;
                        probAnswer = temp * n3;
                        break;
                    case 3:
                        temp = n1 / n2;
                        probAnswer = temp / n3;
                        break;
                    default:
                        probAnswer = -1.0;
                        break;
                }
                break;
            default:
                probAnswer = -1.0;
                break;
        }

        return probAnswer;

    }

}
